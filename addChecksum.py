#!/usr/bin/env python
# coding: utf-8

# This file is part of Adblock Plus <https://adblockplus.org/>,
# Copyright (C) 2006-2016 Eyeo GmbH
#
# Adblock Plus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Adblock Plus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

#############################################################################
# This is a reference script to add checksums to downloadable               #
# subscriptions. The checksum will be validated by Adblock Plus on download #
# and checksum mismatches (broken downloads) will be rejected.              #
#                                                                           #
# To add a checksum to a subscription file, run the script like this:       #
#                                                                           #
#   python addChecksum.py < subscription.txt > subscriptionSigned.txt       #
#                                                                           #
# Note: your subscription file should be saved in UTF-8 encoding, otherwise #
# the operation will fail.                                                  #
#                                                                           #
#############################################################################

import sys
import re
import codecs
import hashlib
import base64

checksumRegexp = re.compile('^\s*!\s*checksum[\s\-:]+([\w\+\/=]+).*\n', re.I | re.M)


def addChecksum(data):
    checksum = calculateChecksum(data)
    data = re.sub(checksumRegexp, '', data)
    data = re.sub('(\r?\n)', '\r! Checksum: %s\r' % checksum, data, 1)
    return data


def calculateChecksum(data):
    md5 = hashlib.md5()
    md5.update(normalize(data).encode('utf-8'))
    md5str = md5.digest()#.encode('utf-8')
    print(base64.b64encode(md5str))
    return base64.b64encode(md5str).decode().rstrip('=')


def normalize(data):
    data = re.sub('\r', '', data)
    data = re.sub('\n+', '\n', data)
    data = re.sub(checksumRegexp, '', data)
    return data


def readStream(stream):
    #reader = codecs.getreader('utf8')(stream)

    fo = open(stream, "r+", encoding='utf8')
    #reader = codecs.getreader('utf-8')(stream)
    #print(fo.read())
    try:
        return fo.read()
    #except Exception, e:
    except Exception as e:
        raise Exception('Failed reading data, most likely not encoded as UTF-8:\n%s' % e)

def saveFile(Filename,data):
    # 打开一个文件
    f = open(Filename, "w", encoding='utf8')

    num = f.write(data)
    #print(num)
    # 关闭打开的文件
    f.close()

if __name__ == '__main__':
    if sys.platform == "win32":
        import os
        import msvcrt
        msvcrt.setmode(sys.stdout.fileno(), os.O_BINARY)

    #print(sys.argv[1])
    data = addChecksum(readStream(sys.argv[1]))
    saveFile(sys.argv[2], data)
